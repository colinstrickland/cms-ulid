(in-package :cms-ulid)

(defstruct ulid
  (time 0 :type (unsigned-byte 48))
  (random 0 :type (unsigned-byte 80)))

(defconstant +80-bit-maxint+ (parse-integer "ffffffffffffffffffff" :radix 16))

(declaim (ftype (function () (unsigned-byte 48)) unix-ts-ms))
(defun unix-ts-ms ()
  "unix-ts-ms returns the unix timestamp to millisecond precision"
  (let ((time (local-time:now)))
    (+ (* 1000 (local-time:timestamp-to-unix time))
       (local-time:timestamp-millisecond time))))

(declaim (ftype (function (ulid) (values (vector (unsigned-byte 8) 16) &optional)) ulid->bytes-seq))
(defun ulid->bytes-seq (u)
  "ulid->bytes-seq converts the ulid binary struct to a sequence of bytes"
  (let ((bytes (make-array 16 :element-type '(unsigned-byte 8))))
    (setf (subseq bytes 0 6) (int->bytes (slot-value u 'time) 6))
    (setf (subseq bytes 6 16) (int->bytes (slot-value u 'random) 10))
    bytes))

(declaim (ftype (function ((integer) (integer)) list) int->bytes))
(defun int->bytes (x n)
  "convert an integer x into an n length byte sequence"
  (let ((max (- (expt 2 (* 8 n)) 1))) ;; check width will fit
    (when (not (>= max x 0))
      (error 'type-error :expected-type `(integer 0 ,max)
                         :datum x
                         :context "when binding X")))
  (loop :for i :from (- n 1) :downto 0
        :collecting (ldb (byte 8 (* i 8)) x)))


(declaim (ftype (function ((integer 0 255)) (values list)) byte8->bits))
(defun byte8->bits (x)
  "convert an unsigned integer x representing a single byte8 value into a sequence of 8 bits"
  (loop :for i :from 7 :downto 0
        :collecting (ldb (byte 1 i) x)))

(declaim (ftype (function ((vector (unsigned-byte 8))) (string)) bytes->base32))
(defun bytes->base32 (bv)
  "bytes->base32 takes a bytes vector and produces a crockford base32 encoded string"
  (let* ((bits (bit-smasher:octets->bits bv))
         (chunk (rem (length bits) 5)))
    (when (> chunk 0)
      (let ((pad (make-array (- 5 chunk) :element-type 'bit :initial-element 0)))
        (setf bits (concatenate 'bit-vector pad bits))))
    (let ((alphabet "0123456789ABCDEFGHJKMNPQRSTVWXYZ"))
      (map 'string
           (lambda (n) (aref alphabet (bit-smasher:bits->int n)))
           (serapeum:batches bits 5)))))

(deftype random-function-ftype () '(function (integer) integer))
(declaim (ftype (function (&key (:time (or (unsigned-byte 48) null))
                                (:random random-function-ftype))
                          (values string (vector (unsigned-byte 8) 16) ulid))
                ulid))
(let ((last (make-ulid :time 0 :random 0)))
  (defun ulid (&key time  (random #'crypto:strong-random))
    "ulid makes a new ulid

     returned as a 3 values tuple
     - string
     - bytes sequence
     - the binary struct

     args:
     :time - specify a timestamp to use
     :random - a random function that takes an int n and returns  int > 0 and < n
               (defaults to #'crypto:strong-random"

    (let ((now (or time (unix-ts-ms)))
          (u last))
      (if (equal now (slot-value last 'time))
          (progn (incf (slot-value u 'random)) ;; add 1 to random of previous ulid
                 (check-type (slot-value u 'random) (unsigned-byte 80)))
          (progn
            (setf u (make-ulid :time now :random (funcall random +80-bit-maxint+)))
            (setf last u)))
      (let ((bytes (ulid->bytes-seq u)))
        (values (bytes->base32 bytes) bytes u)))))
