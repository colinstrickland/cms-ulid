(in-package :cl-user)
(defpackage :cms-ulid/tests (:use :cl :fiveam))
(in-package :cms-ulid/tests)

(def-suite all-tests)
(def-suite ulid :in all-tests)
(def-suite binary-utils  :in all-tests)

(in-suite ulid)
(test test-harness-is-working
  (is (if t t)))

(test test-ulid
  (is (cms-ulid:ulid)))

(test test-ulid-length
  (is (= 26 (length (cms-ulid:ulid)))))

(test test-ulid-sort
  (let ((slow-ulids (loop for i upto 5
                          collecting (cms-ulid:ulid)
                          do (sleep 0.01)))
        (fast-ulids (loop for i upto 1000
                          collecting (cms-ulid:ulid))))
    (is (and
          (every #'string-lessp slow-ulids (rest slow-ulids))
          (every #'string-lessp fast-ulids (rest fast-ulids))))))

(test test-static-ulid
  (multiple-value-bind (u b s) (cms-ulid:ulid :time 1634263671121 :random
                                              (lambda (n) (declare (ignore n)) 1))
    (is (and (string-equal u "01FJ0V98AH0000000000000001")
             (equalp b #(1 124 129 180 161 81 0 0 0 0 0 0 0 0 0 1))
             (equal (slot-value s 'time)  1634263671121)
             (equal (slot-value s 'random) 1)))))

(test test-ulid-bounds
  (let* ((max-time (- (expt 2 48) 1))
         (u (cms-ulid:ulid :time max-time
                           :random (lambda (n) cms-ulid::+80-bit-maxint+))))

    (is(string-equal u "7ZZZZZZZZZZZZZZZZZZZZZZZZZ")) ; biggest permissable ulid in string rep
    ;; another ulid with the same time cannot be made, it should overflow
    (signals type-error (cms-ulid:ulid :time max-time))))


(in-suite binary-utils)

(test test-int->bytes2
  (is-every (lambda (a b) (equal (apply 'cms-ulid::int->bytes a) b))
    ('(1 8)           '(0 0 0 0 0 0 0 1))
    ('(1 4)           '(0 0 0 1))
    ('(10001 4)       '(0 0 39 17))
    ('(2345675 4)     '(0 35 202 203)))
  (signals type-error (cms-ulid::int->bytes 256 1))
  (signals type-error (cms-ulid::int->bytes -1 10)))

(test test-bytes8->bits
  (is-every (lambda (a b) (equal (cms-ulid::byte8->bits a) b))
    (0               '(0 0 0 0 0 0 0 0))
    (1               '(0 0 0 0 0 0 0 1))
    (2               '(0 0 0 0 0 0 1 0))
    (8               '(0 0 0 0 1 0 0 0))
    (64              '(0 1 0 0 0 0 0 0))
    (128             '(1 0 0 0 0 0 0 0))
    (255             '(1 1 1 1 1 1 1 1)))
  ;; bounds checks - we should get type errors from trying to use
  ;; out of spec ints as bytes
  (signals type-error (cms-ulid::byte8->bits -1))
  (signals type-error (cms-ulid::byte8->bits 256)))
