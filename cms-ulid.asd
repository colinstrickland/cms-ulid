(defsystem "cms-ulid"
  :description "cms-ulid: a simple implementation of the ULID scheme for sortable unique identifiers"
  :version "0.0.1"
  :author "cms <cms@beatworm.co.uk>"
  :license "see LICENSE.md"
  :depends-on ("local-time"
               "ironclad"
               "bit-smasher"
               "serapeum")
  :components ((:file "package")
               (:file "ulid"))
  :in-order-to ((test-op (test-op "cms-ulid/tests"))))

(defsystem "cms-ulid/tests"
  :depends-on ("fiveam" )
  :pathname "tests"
  :components ((:file "test-ulid"))
  :perform (test-op (op c)
                    (symbol-call :fiveam :run! (find-symbol* :all-tests :cms-ulid/tests))))
