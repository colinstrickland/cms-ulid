# cms-ulid

## v0.01

Building a first release with the clci pipeline. Library is functionally complete and testable enough to use

## v0.1

I can't figure out how to get the tags to work - just pushing a tag doesn't seem to trigger anything

## v0.1.1

Trying once again, last time I forgot to update the changelog

## v0.1.2

Fixed bad YAML in gitlab-ci 

## v0.1.3

Perhaps we need to just push a tag _after_ we pushed a changelog
