# common lisp implementation of ULID

## WHAT

Here is a straightforward, implementation of the [ULID specifiation](https://github.com/ulid/spec) for lexicographically sortable reasonably unique identifiers.


## WHY

ULIDs are cool, I wanted to use them for something else I was experimenting with, and I couldn't immediately find a workable lisp implementation. There is also an implementation of [Crockford's Base32 variant](https://www.crockford.com/base32.html) which ULID requires, something else I could not find a straightforward lisp implementation for.

## HOW

it relies on ironclad for good randomness.

one public function, ulid. It returns 3 values
- a ULID string
- a bytes vector of the binary form
- the binary struct

`ulid` has two optional arguments

`:time` - specify your own timestamp, milliseconds since epoch
`:random` - specify your own RNG , e.g. `(lambda (x) (random x))`

```
CL-USER> (cms-ulid:ulid)
"01GHGA3GJ65CHWRVQTC8C7AD4P"
#(1 132 96 161 194 70 43 35 204 110 250 98 24 117 52 150)
#S(CMS-ULID:ULID :TIME 1668068524614 :RANDOM 203722125792704978039958)
CL-USER>
```

You can use it with asdf - put the source directory in your project registry and load it with `(asdf:load-project "cms-ulid")`. You can run the test suite with `(asdf:test-project "cms-ulid")`

## TODO

- add parse / decode
- maybe extract base32 methods
